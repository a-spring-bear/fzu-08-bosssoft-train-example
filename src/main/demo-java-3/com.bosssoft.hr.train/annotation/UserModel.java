package com.bosssoft.hr.train.annotationTest;


/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:42
 * @since
 **/
@Table("t_user")
public class UserModel extends BaseModel {
    @Column("name")
    private String name;

    @Id("id")
    private Integer Id;

    @Column("age")
    private Integer age;


    public UserModel() {
    }

    public UserModel(Integer Id, String name, Integer age) {
        this.Id = Id;
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getID() {
        return Id;
    }

    public void setID(Integer ID) {
        this.Id = ID;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }


}

package com.bosssoft.hr.train.collection;


/**
 *  ArrayList优势在随机访问和尾部添加 劣势中间插入
 */
public interface ArrayListExmaple<T> extends ListExample<T> {

    /**
     *
     * @param position
     * @param node
     * @return
     */
     boolean insert(Integer position,T node);

    /**
     * 中间删除
     * @param position
     * @return
     */
    @Override
    boolean remove(Integer position);


}

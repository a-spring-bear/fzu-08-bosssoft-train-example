package com.bosssoft.hr.train.collection;



/**
 * @description: 如果需要添加HashMap的特有的接口测试就需要在该接口中添加
 * 需要测试的方法
 * @author: Administrator
 * @create: 2020-05-28 20:45
 * @since
 **/
public interface HashMapExample<Role, Resource> extends MapExample<Role, Resource> {
}

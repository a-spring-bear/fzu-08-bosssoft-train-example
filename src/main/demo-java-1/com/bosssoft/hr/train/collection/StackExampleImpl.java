package com.bosssoft.hr.train.collection;


import com.bosssoft.hr.train.web.pojo.User;
import java.util.Stack;

/**
 * @description:
 * @author:zyz
 * @time:2020/5/29--16:05
 */
public class StackExampleImpl implements StackExample<User> {
    Stack<User> users = new Stack<>();

    @Override
    public User push(User item) {
        return users.push(item);
    }

    @Override
    public User pop() {
        return users.pop();
    }

//    查看栈顶的元素而不移除
    @Override
    public User peek() {
        return users.peek();
    }

    @Override
    public boolean empty() {
        return users.empty();
    }

    @Override
    public int search(User user) {
        return users.search(user);
    }

    public Stack<User> getUsers() {
        return users;
    }

    public void setUsers(Stack<User> users) {
        this.users = users;
    }
}
